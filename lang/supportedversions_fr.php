<?php

$GLOBALS[$GLOBALS['idx_lang']] = [
	'branch' => 'Branche',
	'active_support' => 'Maintenance active',
	'active_support_definition' => 'Une version qui est activement prise en charge.
		Les bogues signal&eacute;s et les probl&egrave;mes de s&eacute;curit&eacute; sont corrig&eacute;s
		et des mises &agrave; jour r&eacute;guli&egrave;res sont effectu&eacute;es.',
	'active_support_until' => 'Maintenance active jusqu\'au',
	'security_fix' => 'Correctifs de s&eacute;curit&eacute; seulement',
	'security_fix_definition' => 'Une version prise en charge uniquement pour les probl&egrave;mes
		de s&eacute;curit&eacute; critiques.
		Les publications ne sont effectu&eacute;es qu\'au besoin.',
	'security_support_until' => 'Correctifs de s&eacute;curit&eacute; jusqu\'au',
	'end_of_life' => 'Fin de vie',
	'end_of_life_definition' => 'Une version qui n\'est plus prise en charge.
		Les utilisateurs de cette version doivent mettre &agrave; jour d&egrave;s que possible,
		car ils peuvent être expos&eacute;s &agrave; des vuln&eacute;rabilit&eacute;s de s&eacute;curit&eacute;
		non corrig&eacute;es.',
	'initial_release' => 'Premi&egrave;re publication',
	'unreleased' => 'Non publi&eacute;e',
	'unreleased_definition' => 'Une version qui n\'a pas encore &eacute;t&eacute; publi&eacute;e.',
	'php_compatibility' => 'Compatibilit&eacute; PHP',
	'last_release' => 'Derni&egrave;re publication',
	'latest_releases' => 'Derni&egrave;res versions',
	'current_page' => 'version maintenue',
	'eol_page' => 'Un tableau de la fin de vie des branches est disponible.',
	'released_at' => 'Publi&eacute;e le',
	'announcement' => 'Annonce',
	'changelog' => 'Changelog',
	'download' => 'T&eacute;l&eacute;chargement',
	'download_size' => 'Taille',
	'freespace' => 'Espace disque (hors base de données)',
	'ram' => 'Mémoire RAM',
	'system_needs'  => 'Besoin système minimum',
	'sql' => 'Base de données',
	'image_processing' => 'Traitement d\'images',
	'required' => 'Requis',
	'suggest' => 'Suggestions',
	'provided' => 'Fourni',
	'php_extensions' => 'Extensions PHP',
	'no_future_version' => 'Pas de future version pr&eacute;vue actuellement.',
	'no_maintained_version' => 'Pas de version maintenue actuellement.',
];
