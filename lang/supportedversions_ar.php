<?php

$GLOBALS[$GLOBALS['idx_lang']] = [
	'branch' => 'الفرع',
	'active_support' => 'صيانة نشطة',
	'active_support_definition' => 'إصدار تتم صيانته بشكل نشط ومتواصل.
		يتم تصحيح الأخطاء المبلغ عنها وثغرات الأمان
		كما يتم نشر تحديثات متواصلة.',
	'active_support_until' => 'صيانة نشطة حتى',
	'security_fix' => 'تصحيحات أمان فقط',
	'security_fix_definition' => 'إصدار مصان لتصحيح 
		ثغرات الأمان المهمة فقط.
		ويتم النشر عند الحاجة فقط.',
	'security_support_until' => 'تصحيحات أمان حتى',
	'end_of_life' => 'نهاية الخدمة',
	'end_of_life_definition' => 'إصدار لم يعد مصان.
		ويتحتم على مستخدمي هذا الإصدار التحديث في أول فرصة
		لأنهم قد يتعرضون الى مشاكل أمان 
		غير مصححة.',
	'initial_release' => 'أول نشرة',
	'unreleased' => 'غير منشور',
	'unreleased_definition' => 'إصدار لم يتم نشره يعد.',
	'php_compatibility' => 'التوافق مع PHP',
	'last_release' => 'أحدث نشرة',
	'latest_releases' => 'أحدث الاصدارات',
	'current_page' => 'الاصدار المصان',
	'eol_page' => 'هناك جدول نهاية خدمة متوافر.',
	'released_at' => 'نشر في',
	'announcement' => 'إعلان',
	'changelog' => 'التغييرات',
	'download' => 'تحميل',
	'download_size' => 'الحجم',
];
