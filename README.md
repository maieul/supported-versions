# Supported Versions

SPIP Plugin backport of [PHP Supported Versions](http://php.net/supported-versions.php)

[![pipeline status](https://gitlab.com/JamesRezo/supported-versions/badges/master/pipeline.svg)](https://gitlab.com/JamesRezo/supported-versions/-/commits/master)

[![coverage report](https://gitlab.com/JamesRezo/supported-versions/badges/master/coverage.svg)](https://gitlab.com/JamesRezo/supported-versions/-/commits/master)

## Models

- `<supportedversions|table>` : a table of actively supported versions
- `<supportedversions|calendar>` : a calendar view of the table above
- `<supportedversions|legend>` : colors and style explanations
- `<supportedversions|releases>` : list of releases in JSON
- `<supportedversions|latest>`: list of the latest releases of active branches
- `<supportedversions|eol>`: list of the end of life branches
- `<supportedversions|compatibility>`: table matrix of SPIP/PHP Compatibility
- `<supportedversions|release_notes>`: list of releases by date
- `<supportedversions|pre_releases>` : list of pre-releases in JSON
- `<supportedversions|stable>` : Gives informations of the last stable version released with `info` parameter (version, date or url)
- `<supportedversions|spiploaderlist_X>` : JSON for `spip_loader.php` where X is the API version.
- `<supportedversions|configuration>` : list of required, suggested and provided technologies

## Filters

- `|SupportedVersions::getBranchValues` : Array of branch data for a given release string
- `|SupportedVersions::getBranchesFromState` Array of branch data for a given state string
- `|SupportedVersions::branchesByState` Arrray of branch data for a given list of states
- `|SupportedVersions::configuration` Converts a list of elements in a string like `element1, element2 v1`

## Other usage

Calling `/spip.php?page=supported-versions.svg` renders a calendar in svg format.

Calling `/spip.php?page=releases.json` renders a JSON formatted of releases.

Calling `/spip.php?page=pre_releases.json` renders a JSON formatted of alpha, beta and RC pre-releases.

Calling `/spip_loader.api` or `/spip_loader.api/1` for apache httpd server with rewrite mode and `.htaccess` default file exposes the `spip_loader_list.json` JSON. First argument, like `/1` is the `api` parameter.

## .htaccess

```apache
RewriteRule ^supported-versions\.svg$ spip.php?page=supported-versions.svg [QSA,L]
```

and call : `/supported-versions.svg`

```apache
RewriteRule ^releases\.json$ spip.php?page=releases.json [QSA,L]
```

and call : `/releases.json`

```apache
RewriteRule ^pre_releases\.json$ spip.php?page=pre_releases.json [QSA,L]
```

and call : `/pre_releases.json`

```apache
RewriteRule ^spip-dev/INSTALL/spip_loader_list.json$ spip_loader.api [QSA,L]
# Compatibility with spip_loader.php v5.0.1
RewriteRule ^spip-dev/INSTALL/(.*).php.txt$ spip-dev/INSTALL/$1.php [L]
```

and call : `/spip-dev/INSTALL/spip_loader_list.json`

## Thanks

- Hanjo for the Dutch translation
- George for the Arabic translation
- Nicod_ for BEM and so much
- b_b & marcimat for their support
- lspeciale for the Spanish translation
