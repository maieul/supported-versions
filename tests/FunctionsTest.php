<?php

namespace JamesRezo\SupportedVersions\Test;

class FunctionsTest extends TestCase
{
    /**
     * @group pipelines
     * @covers ::supportedversions_insert_head_css
     *
     * @return void
     */
    public function testSupportedVersionsInsertHeadCss()
    {
        $flux = supportedversions_insert_head_css('test');

        $this->assertEquals(
            'test<link rel="stylesheet" type="text/css" media="all" href="css/supported-versions.css" />' . "\n",
            $flux
        );
    }

    /**
     * @group balises
     * @covers ::balise_SUPPORTED_VERSIONS_dist
     *
     * @return void
     */
    public function testBaliseSupportedVersionsDist()
    {
        $champ = new \StdClass();
        $champ = balise_SUPPORTED_VERSIONS_dist($champ);

        $this->assertEquals('(is_array($a = ($GLOBALS["supportedversions"])) ? serialize($a) : "")', $champ->code);
    }
}
