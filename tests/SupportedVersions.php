<?php

namespace JamesRezo\SupportedVersions\Test;

use DateTime;

class SupportedVersions extends \SupportedVersions
{
    public static function getConfig()
    {
        return [
            'config' => self::$config,
            'now' => self::$now,
            'releasesFile' => self::$releasesFile
        ];
    }

    public static function unsetConfig()
    {
        self::$config = null;
        self::$now = null;
        self::$releasesFile = null;
    }

    public static function setConfig(array $config = [], $now = '', $releasesFile = 'tests/releases/init.json')
    {
        self::$config = $config;
        self::$now = new DateTime($now);
        self::$releasesFile = $releasesFile;

        self::compute();
    }
}
