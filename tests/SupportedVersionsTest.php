<?php

namespace JamesRezo\SupportedVersions\Test;

/**
 * @covers \SupportedVersions
 */
class SupportedVersionsTest extends TestCase
{
    public function dataSupportedVersionsState()
    {
        return [
            'release-pas-prévue' => [
                'not-planned',
                [
                    'initial_release' => '',
                    'active_support' => '',
                    'eol' => '',
                ],
            ],
            'release-pas-sortie' => [
                'future',
                [
                    'initial_release' => '1971-12-01',
                    'active_support' => '',
                    'eol' => '',
                ],
            ],
            'release-toujours-stable' => [
                'stable',
                [
                    'initial_release' => '1971-01-01',
                    'active_support' => '',
                    'eol' => '',
                ],
            ],
            'release-bientot-en-maintenance' => [
                'stable',
                [
                    'initial_release' => '1971-01-01',
                    'active_support' => '1971-11-06',
                    'eol' => '',
                ],
            ],
            'release-en-maintenance' => [
                'security',
                [
                    'initial_release' => '1971-01-01',
                    'active_support' => '1971-11-03',
                    'eol' => '',
                ],
            ],
            'release-plus-maintenue' => [
                'eol',
                [
                    'initial_release' => '1971-01-01',
                    'active_support' => '1971-11-03',
                    'eol' => '1971-11-04',
                ],
            ],
        ];
    }

    /**
     * @group filtres
     * @dataProvider dataSupportedVersionsState
     *
     * @param string $expected
     * @param array $release
     * @return void
     */
    public function testSupportedVersionsState($expected, $release)
    {
        //Given
        SupportedVersions::setConfig(
            $this->calendarConfig,
            $this->now,
        );

        //When
        $state = SupportedVersions::state($release);

        //Then
        $this->assertEquals($expected, $state);
    }

    public function dataSupportedVersionsInit()
    {
        return [
            '1-an-avant-annee-courante' => [
                [
                    '1970-01-01',
                    '1971-01-01',
                ],
                [
                    'calendar' => [
                        'min_year' => 'P1Y',
                        'max_year' => 'P1Y',
                    ],
                ],
            ],
            '1-an-avant-annee-courante-1-an-après' => [
                [
                    '1970-01-01',
                    '1971-01-01',
                    '1972-01-01',
                ],
                [
                    'calendar' => [
                        'min_year' => 'P1Y',
                        'max_year' => 'P2Y',
                    ],
                ],
            ],
        ];
    }

    /**
     * @group internal
     * @dataProvider dataSupportedVersionsInit
     *
     * @param string $expected
     * @param array $config
     * @return void
     */
    public function testSupportedVersionsInit($expected, $config)
    {
        //Given
        SupportedVersions::setConfig($config, $this->now);

        //When
        $years = SupportedVersions::years();

        //Then
        $this->assertEquals($expected, $years);
    }

    public function dataSupportedVersionsBranchesToShow()
    {
        return [
            'stables-seulement'  =>  [
                ['0.9', '1.1', '2.0', '2.1', '4.0', '4.1'],
                false,
            ],
            'toutes-versions-dans-l-intervalle-de-temps'  =>  [
                ['0.8', '0.9', '1.0', '1.1', '2.0', '2.1', '4.0', '4.1'],
                true,
            ],
        ];
    }

    /**
     * @group criteres
     * @dataProvider dataSupportedVersionsBranchesToShow
     *
     * @param array $expected
     * @param bool $eol
     * @return void
     */
    public function testSupportedVersionsBranchesToShow($expected, $eol)
    {
        //Given
        SupportedVersions::setConfig(
            $this->calendarConfig,
            $this->now,
        );

        //When
        $branches = SupportedVersions::branchesToShow($eol);

        //Then
        $this->assertEquals($expected, array_reduce(
            $branches,
            function ($branchNames, $branch) {
                $branchNames[] = $branch['branch'];

                return $branchNames;
            },
            []
        ));
    }

    public function dataPhpMatrix()
    {
        return [
            'without-eol' => [[
                '4.0' => [
                    '1.0' => true,
                    '2.0' => false,
                ],
                '4.1' => [
                    '1.0' => true,
                    '2.0' => true,
                ],
            ], false],
            'with-eol' => [[
                '1.0' => [
                    '1.0' => true,
                    '2.0' => false,
                ],
                '4.0' => [
                    '1.0' => true,
                    '2.0' => false,
                ],
                '4.1' => [
                    '1.0' => true,
                    '2.0' => true,
                ],
            ], true],
        ];
    }

    /**
     * @group criteres
     * @dataProvider dataPhpMatrix
     */
    public function testPhpMatrix($expected, $eol)
    {
        //Given
        SupportedVersions::setConfig(
            $this->calendarConfig,
            $this->now,
        );

        //When
        $matrix = SupportedVersions::phpMatrix($eol);

        //Then
        $this->assertEquals($expected, $matrix);
    }

	public function dataGetBranchValues()
	{
		return [
			'no-match' => [
				[
					'branch' => '',
					'initial_release' => '',
					'active_support' => '',
					'eol' => '',
					'technologies' => [
						'require' => [
							'php' => []
						],
					],

					'releases' => [],
				],
				'no-match',
			],
			'match_but_not_exists' => [
				[
					'branch' => '',
					'initial_release' => '',
					'active_support' => '',
					'eol' => '',
					'technologies' => [
						'require' => [
							'php' => []
						],
					],
					'releases' => [],
				],
				'6.10.0',
			],
			'match_and_exists' => [
				[
					'branch' => '1.0',
					'initial_release' => '1971-02-03',
					'active_support' => '1971-06-06',
					'eol' => '1971-10-07',
					'technologies' => [
						'require' => [
							'php' => ['1.0']
						],
					],
				],
				'1.0.0-alpha'
			]
		];
	}

    /**
     * @group filtres
     * @dataProvider dataGetBranchValues
	 */
	public function testGetBranchValues($expected, $release)
	{
		//Given
        SupportedVersions::setConfig(
            $this->calendarConfig,
            $this->now,
        );

		//When
        $values = SupportedVersions::getBranchValues($release);

        //Then
        $this->assertEquals($expected, $values);
	}

	public function dataGetBranchesFromState()
	{
		return [
			'randomstring' => [
				[''],
				'randomstring',
			],
			'future' => [
				['3.0', '4.0', '4.1'],
				'future',
			],
			'stable' => [
				['0.1', '0.9', '2.0', '2.1'],
				'stable',
			],
			'security' => [
				['1.1'],
				'security',
			],
			'eol' => [
				['0.1', '0.8', '1.0'],
				'eol',
			],
		];
	}

    /**
     * @group filtres
     * @dataProvider dataGetBranchesFromState
	 */
	public function testGetBranchesFromState($expected, $state)
	{
		//Given
        SupportedVersions::setConfig(
            $this->calendarConfig,
            $this->now,
        );

		//When
        $branches = SupportedVersions::getBranchesFromState($state);
		$branches = array_reduce($branches, function ($branchNames, $branch) {
			$branchNames[] = $branch['branch'];

			return $branchNames;
		},[]);

        //Then
        $this->assertEquals($expected, $branches);
	}


    public function dataSupportedVersionsStateOrGradient()
    {
        return [
            '1.0-unknown-state' => [
                '',
                '1.0',
                'unknonwn',
            ],
            '0.0-unknown-branch' => [
                '',
                '0.0',
                'stable',
            ],
            '0.1-stable-before-svg' => [
                '',
                '0.1',
                'stable',
            ],
            '0.1-eol-before-svg' => [
                '',
                '0.1',
                'security',
            ],
            '1.0-eol-in-svg' => [
                'security',
                '1.0',
                'security',
            ],
            '1.0-stable-in-svg' => [
                'stable',
                '1.0',
                'stable',
            ],
            '1.0-no-future' => [
                '',
                '1.0',
                'future',
            ],
            '3.0-future-after-svg' => [
                '',
                '3.0',
                'future',
            ],
            '3.0-security-after-svg' => [
                '',
                '3.0',
                'security',
            ],
            '3.0-stable-after-svg' => [
                '',
                '3.0',
                'stable',
            ],
            '1.1-security-gradient' => [
                'security-gradient',
                '1.1',
                'security',
            ],
            '2.1-stable-gradient' => [
                'stable-gradient',
                '2.1',
                'stable',
            ],
            '4.1-future' => [
                'future',
                '4.1',
                'future',
            ],
            '4.1-future-is-not-stable' => [
                '',
                '4.1',
                'stable',
            ],
            '4.0-future-gradient' => [
                'future-gradient',
                '4.0',
                'future',
            ],
        ];
    }

    /**
     * @group filtres
     * @group svg
     * @dataProvider dataSupportedVersionsStateOrGradient
     *
     * @param string $expected
     * @param string $branch
     * @param string $state
     * @return void
     */
    public function testSupportedVersionsStateOrGradient($expected, $branch, $state)
    {
        //Given
        SupportedVersions::setConfig(
            $this->calendarConfig,
            $this->now,
        );

        //When
        $cssClass = SupportedVersions::stateOrGradient($branch, $state);

        //Then
        $this->assertEquals($expected, $cssClass);
    }

    public function dataSupportedVersionsHorizCoord()
    {
        return [
            'date-is-empty' => [
                812,
                '',
            ],
            'date-in-svg' => [
                589,
                '1971-11-30',
            ],
            'date-before-svg' => [
                100,
                '1969-01-01',
            ],
            'date-after-svg' => [
                812,
                '1974-01-01',
            ],
        ];
    }

    /**
     * @group filtres
     * @group svg
     * @dataProvider dataSupportedVersionsHorizCoord
     *
     * @param float $expected
     * @param string $date
     * @return void
     */
    public function testSupportedVersionsHorizCoord($expected, $date)
    {
        //Given
        SupportedVersions::setConfig(
            array_merge($this->svgConfig, $this->calendarConfig),
            $this->now
        );

        //When
        $horizCoord = SupportedVersions::horizCoord($date);

        //Then
        $this->assertEquals($expected, $horizCoord);
    }

    public function dataSupportedVersionsTop()
    {
        return [
            'branch-not-set' => [
                48,
                '',
            ],
            'branch-not-exist' => [
                48,
                '0.2',
            ],
            'branch-exists' => [
                304,
                '2.0',
            ],
        ];
    }

    /**
     * @group svg
     * @dataProvider dataSupportedVersionsTop
     *
     * @param int $expected
     * @param string $branch
     * @return void
     */
    public function testSupportedVersionsTop($expected, $branch)
    {
        //Given
        SupportedVersions::setConfig(
            array_merge($this->svgConfig, $this->calendarConfig),
            $this->now
        );

        //When
        $actual = SupportedVersions::top($branch);

        //Then
        $this->assertEquals($expected, $actual);
    }

    public function dataSupportedVersionsRectWidth()
    {
        return [
            'state-unknown' => [
                '',
                '0.9',
                'unknown'
            ],
            'branch-unknown' => [
                '',
                '0.0',
                'stable',
            ],
            'avant-le-cadre-après-le-cadre' => [
                712,
                '0.9',
                'stable'
            ],
            'avant-le-cadre-dans-le-cadre' => [
                278,
                '0.8',
                'stable',
            ],
            'dans-le-cadre-après-le-cadre' => [
                261,
                '1.1',
                'security',
            ],
            'dans-le-cadre-dans-le-cadre' => [
                87,
                '1.0',
                'stable',
            ],
        ];
    }

    /**
     * @group svg
     * @dataProvider dataSupportedVersionsRectWidth
     *
     * @param float $expected
     * @param string $start
     * @param string $end
     * @return void
     */
    public function testSupportedVersionsRectWidth($expected, $branch, $state)
    {
        //Given
        SupportedVersions::setConfig(
            array_merge($this->svgConfig, $this->calendarConfig),
            $this->now
        );

        //When
        $actual = SupportedVersions::rectWidth($branch, $state);

        //Then
        $this->assertEquals($expected, $actual);
    }

	public function dataConfiguration()
	{
		return [
			[
				'test, othertestv1',
				['test' => '*', 'othertest' => '1'],
			],
			[
				'test, (one way | another)',
				['test' => '*', 'or' => ['one way' => '*', 'another' => '*']],
			],
		];
	}

    /**
     * @group filtres
     * @dataProvider dataConfiguration
     */
	public function testConfiguration($expected, $configuration)
	{
		//Given a $configuration

		//When
		$actual = SupportedVersions::configuration($configuration, 'v');

		//Then
        $this->assertEquals($expected, $actual);
	}
}
